# aws-s3

LittleURL redirection engine powered by [Amazon S3](https://docs.aws.amazon.com/AmazonS3/latest/userguide/how-to-page-redirect.html)